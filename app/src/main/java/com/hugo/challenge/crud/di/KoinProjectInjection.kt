package com.hugo.challenge.crud.di

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class KoinProjectInjection {
    companion object {
        fun initialize(context: Application) {
            startKoin {
                androidContext(context)
                modules(listOf(
                    databaseModule,
                    repositoryModule,
                    useCasesModule,
                    viewModelModule
                ))
            }
        }
    }
}