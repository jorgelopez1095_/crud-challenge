package com.hugo.challenge.crud.interactor.usecase

import com.hugo.challenge.crud.data.UserEntity

interface AddUserUseCase {
    suspend fun invoke(user: UserEntity)
}