package com.hugo.challenge.crud

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.hugo.challenge.crud.databinding.ActivityCrudBinding

class CrudActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCrudBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCrudBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}