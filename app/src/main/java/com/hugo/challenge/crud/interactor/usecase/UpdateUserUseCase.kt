package com.hugo.challenge.crud.interactor.usecase

interface UpdateUserUseCase {
    suspend fun invoke(userId: Long, userName: String, userSurName: String)
}