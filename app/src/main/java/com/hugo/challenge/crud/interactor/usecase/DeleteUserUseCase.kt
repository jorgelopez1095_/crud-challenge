package com.hugo.challenge.crud.interactor.usecase

interface DeleteUserUseCase {
    suspend fun invoke(userId: Long)
}