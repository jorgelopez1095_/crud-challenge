package com.hugo.challenge.crud

import android.app.Application
import com.hugo.challenge.crud.di.KoinProjectInjection

class CrudApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        KoinProjectInjection.initialize(this)
    }
}