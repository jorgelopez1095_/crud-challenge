package com.hugo.challenge.crud

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import com.hugo.challenge.crud.databinding.FragmentSplashScreenBinding
import com.hugo.challenge.crud.models.SplashViewModel
import com.hugo.challenge.crud.utils.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashScreen: BaseFragment<SplashViewModel, FragmentSplashScreenBinding>() {

    override val viewModel: SplashViewModel by viewModel()

    override fun layoutId(): Int = R.layout.fragment_splash_screen

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.let { binding.viewModel = it }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() { /* Do nothing */ }
            }
        )
        viewModel.loadingSplashScreen()
    }
}
