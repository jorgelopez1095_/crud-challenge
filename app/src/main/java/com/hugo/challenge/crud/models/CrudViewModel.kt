package com.hugo.challenge.crud.models

import androidx.lifecycle.MutableLiveData
import com.hugo.challenge.crud.data.UserEntity
import com.hugo.challenge.crud.interactor.usecase.AddUserUseCase
import com.hugo.challenge.crud.interactor.usecase.DeleteUserUseCase
import com.hugo.challenge.crud.interactor.usecase.DeleteUsersUseCase
import com.hugo.challenge.crud.interactor.usecase.GetUserUseCase
import com.hugo.challenge.crud.interactor.usecase.GetUsersUseCase
import com.hugo.challenge.crud.interactor.usecase.UpdateUserUseCase
import com.hugo.challenge.crud.utils.BaseViewModelImpl

class CrudViewModel (
    private val createUser: AddUserUseCase,
    private val updateUser: UpdateUserUseCase,
    private val deleteUser: DeleteUserUseCase,
    private val deleteUsers: DeleteUsersUseCase,
    private val getUsers: GetUsersUseCase,
    private val getUser: GetUserUseCase
): BaseViewModelImpl() {

    val users = MutableLiveData<List<UserEntity>>()
    val userSelected = MutableLiveData<UserEntity>()

    init {
        exec {
            users.postValue(getUsers.invoke())
        }
    }

    fun resetUserSelected() {
        userSelected.postValue(null)
    }

    fun refreshUsersInformation() = exec {
        getUsers.invoke().let {
            if (it.isNotEmpty()) {
                users.postValue(getUsers.invoke())
            } else {
                users.postValue(listOf())
            }
        }
    }

    fun itemUserSelection(index: Int) {
        userSelected.postValue(users.value?.get(index))
    }

    fun itemUser(index: Int): UserEntity? {
        return users.value?.get(index)
    }

    fun addNewUser(user: UserEntity) = exec {
        createUser.invoke(user)
        users.postValue(getUsers.invoke())
    }

    fun updateInformation(id: Long, name: String, surName: String) = exec {
        getUser.invoke(id).let { user ->
            if (user != null) {
                updateUser.invoke(id, name, surName)
                users.postValue(getUsers.invoke())
            }
        }
    }

    fun removeUser(id: Long) = exec {
        getUser.invoke(id).let { user ->
            if (user != null) {
                deleteUser.invoke(id)
                users.postValue(getUsers.invoke())
            }
        }
    }
}