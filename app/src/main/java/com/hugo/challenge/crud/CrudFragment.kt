package com.hugo.challenge.crud

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.core.widget.doOnTextChanged
import com.hugo.challenge.crud.data.UserEntity
import com.hugo.challenge.crud.databinding.FragmentCrudBinding
import com.hugo.challenge.crud.utils.BaseFragment
import com.hugo.challenge.crud.models.CrudViewModel
import com.hugo.challenge.crud.utils.BaseAdapter
import com.hugo.challenge.crud.utils.subscribe
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.Locale
import kotlin.collections.ArrayList

const val EXEC_CREATE = 0
const val EXEC_UPDATE = 1
const val EXEC_DELETE = 2

class CrudFragment: BaseFragment<CrudViewModel, FragmentCrudBinding>() {

    override val viewModel: CrudViewModel by viewModel()
    override fun layoutId(): Int = R.layout.fragment_crud

    private var userIdSelected: Long = 0

    private var updatedList: List<UserEntity> = listOf()

    private var adapter: BaseAdapter<CrudViewModel, UserEntity>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.let { binding.viewModel = it }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() { /* Do nothing */ }
            }
        )

        viewModel.users.subscribe(viewLifecycleOwner, ::getUsers)
        viewModel.userSelected.subscribe(viewLifecycleOwner) { user ->
            if (user != null) {
                userIdSelected = user.id
                binding.textInputEditTextName.setText(user.name)
                binding.textInputEditTextSurName.setText(user.surName)
            }
        }

        adapter = BaseAdapter(viewModel, R.layout.item_user)

        loadFeature()
    }

    private fun getUsers(users: List<UserEntity>) {
        if (users.isNotEmpty()) {
            updatedList = users
            adapter?.clearCollections()
            adapter?.setData(users.toMutableList())
            binding.recyclerViewUsers.adapter = adapter
            binding.recyclerViewUsers.visibility = View.VISIBLE
            binding.animationEmpty.visibility = View.GONE
        } else {
            updatedList = listOf()
            adapter?.clearCollections()
            binding.recyclerViewUsers.adapter = adapter
            binding.recyclerViewUsers.visibility = View.GONE
            binding.animationEmpty.visibility = View.VISIBLE
        }
    }

    private fun loadFeature() {
        setClickListeners()

        binding.textInputEditTextName.doOnTextChanged { _, _, _, _ ->
            if (binding.textInputLayoutName.isErrorEnabled) {
                binding.textInputLayoutName.isErrorEnabled = false
            }
        }

        binding.textInputEditTextSurName.doOnTextChanged { _, _, _, _ ->
            if (binding.textInputLayoutSurName.isErrorEnabled) {
                binding.textInputLayoutSurName.isErrorEnabled = false
            }
        }

        binding.searchViewUsers.doOnTextChanged { text, _, _, _ ->
            if (text?.length != 0) {
                if (updatedList.isNotEmpty()) {
                    val filtered: MutableList<UserEntity>
                            = filter(updatedList as MutableList<UserEntity>, text.toString())
                    if (filtered.isNotEmpty()) {

                        viewModel.users.value = filtered
                        adapter?.clearCollections()
                        adapter?.setData(filtered.toMutableList())
                        binding.recyclerViewUsers.adapter = adapter

                        binding.recyclerViewUsers.visibility = View.VISIBLE
                        binding.animationEmpty.visibility = View.GONE

                    } else {
                        binding.recyclerViewUsers.visibility = View.GONE
                        binding.animationEmpty.visibility = View.VISIBLE
                    }
                }
            } else {
                viewModel.refreshUsersInformation()
            }
        }
    }

    private fun filter(model: MutableList<UserEntity>, queryString: String): MutableList<UserEntity> {
        val query = queryString.toLowerCase(Locale.getDefault())
        val filteredList = ArrayList<UserEntity>()
        for (result: UserEntity in model) {
            val data = result.name.toLowerCase(Locale.getDefault()) +
                    result.surName.toLowerCase(Locale.getDefault())
            if (data.contains(query)) {
                filteredList.add(result)
            }
        }
        return filteredList
    }

    private fun setClickListeners() {
        // The user needs to set information in both fields.
        binding.buttonCreate.setOnClickListener { doTask(EXEC_CREATE) }
        binding.buttonUpdate.setOnClickListener { doTask(EXEC_UPDATE) }
        binding.buttonDelete.setOnClickListener { doTask(EXEC_DELETE) }
    }

    // 0 -> create, 1 -> update, 2 -> delete.
    private fun doTask(origin: Int) {
        if (isEmptyFields().not()) {
            when (origin) {
                EXEC_CREATE -> {
                    viewModel.addNewUser(
                        UserEntity(
                            id = 0,
                            name = binding.textInputEditTextName.text.toString(),
                            surName = binding.textInputEditTextSurName.text.toString()
                        )
                    )
                    cleanupFields()
                    viewModel.resetUserSelected()
                }
                EXEC_UPDATE -> {
                    viewModel.updateInformation(
                        userIdSelected,
                        binding.textInputEditTextName.text.toString(),
                        binding.textInputEditTextSurName.text.toString()
                    )
                    cleanupFields()
                    viewModel.resetUserSelected()
                }
                EXEC_DELETE -> {
                    viewModel.removeUser(userIdSelected)
                    cleanupFields()
                    viewModel.resetUserSelected()
                }
            }
        } else {
            when {
                binding.textInputEditTextName.text.isNullOrEmpty() -> {
                    setNameError(resources.getString(R.string.err_message_forgot_name))
                }
                binding.textInputEditTextSurName.text.isNullOrEmpty() -> {
                    setSurNameError(resources.getString(R.string.err_message_forgot_surname))
                }
            }
        }
    }

    private fun isEmptyFields(): Boolean {
        return binding.textInputEditTextName.text.isNullOrEmpty() ||
                    binding.textInputEditTextSurName.text.isNullOrEmpty()
    }

    private fun setNameError(message: String) {
        binding.textInputLayoutName.error = message
    }

    private fun setSurNameError(message: String) {
        binding.textInputLayoutSurName.error = message
    }

    private fun cleanupFields() {
        binding.textInputEditTextSurName.text?.clear()
        binding.textInputEditTextName.text?.clear()
    }

}
