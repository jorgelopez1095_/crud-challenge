package com.hugo.challenge.crud.data

import androidx.room.Database
import androidx.room.RoomDatabase

const val DATABASE_NAME = "crud.db"

@Database(
    entities = [UserEntity::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase: RoomDatabase() {
    abstract fun userDao(): UserDao
}