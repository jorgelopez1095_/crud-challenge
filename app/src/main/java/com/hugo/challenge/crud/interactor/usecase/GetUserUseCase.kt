package com.hugo.challenge.crud.interactor.usecase

import com.hugo.challenge.crud.data.UserEntity

interface GetUserUseCase {
    suspend fun invoke(userId: Long): UserEntity?
}