package com.hugo.challenge.crud.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

const val TABLE_USER = "users"
const val EMPTY_STRING = ""

@Entity(tableName = TABLE_USER)
data class UserEntity (
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "name") val name: String = EMPTY_STRING,
    @ColumnInfo(name = "sur_name") val surName: String = EMPTY_STRING
)