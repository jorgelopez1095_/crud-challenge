package com.hugo.challenge.crud.interactor.usecase

import com.hugo.challenge.crud.interactor.repository.UserRepository

class DeleteUserUseCaseImpl (
    private val repository: UserRepository
): DeleteUserUseCase {
    override suspend fun invoke(userId: Long) = repository.deleteUser(userId)
}