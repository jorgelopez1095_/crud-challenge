package com.hugo.challenge.crud.di

import androidx.room.Room
import com.hugo.challenge.crud.models.CrudViewModel
import com.hugo.challenge.crud.data.AppDatabase
import com.hugo.challenge.crud.data.DATABASE_NAME
import com.hugo.challenge.crud.interactor.repository.UserRepository
import com.hugo.challenge.crud.interactor.repository.UserRepositoryImpl
import com.hugo.challenge.crud.interactor.usecase.AddUserUseCase
import com.hugo.challenge.crud.interactor.usecase.AddUserUseCaseImpl
import com.hugo.challenge.crud.interactor.usecase.DeleteUserUseCase
import com.hugo.challenge.crud.interactor.usecase.DeleteUserUseCaseImpl
import com.hugo.challenge.crud.interactor.usecase.DeleteUsersUseCase
import com.hugo.challenge.crud.interactor.usecase.DeleteUsersUseCaseImpl
import com.hugo.challenge.crud.interactor.usecase.GetUserUseCase
import com.hugo.challenge.crud.interactor.usecase.GetUserUseCaseImpl
import com.hugo.challenge.crud.interactor.usecase.GetUsersUseCase
import com.hugo.challenge.crud.interactor.usecase.GetUsersUseCaseImpl
import com.hugo.challenge.crud.interactor.usecase.UpdateUserUseCase
import com.hugo.challenge.crud.interactor.usecase.UpdateUserUseCaseImpl
import com.hugo.challenge.crud.models.SplashViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val databaseModule = module {
    single {
        Room.databaseBuilder(
            androidContext(),
            AppDatabase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }
    factory { get<AppDatabase>().userDao() }
}

val viewModelModule = module {
    viewModel {
        CrudViewModel(
            get(),
            get(),
            get(),
            get(),
            get(),
            get()
        )
    }
    viewModel { SplashViewModel() }
}

val repositoryModule = module {
    factory<UserRepository> { UserRepositoryImpl(get()) }
}

val useCasesModule = module {
    factory<AddUserUseCase> { AddUserUseCaseImpl(get()) }
    factory<GetUsersUseCase> { GetUsersUseCaseImpl(get()) }
    factory<DeleteUsersUseCase> { DeleteUsersUseCaseImpl(get()) }
    factory<DeleteUserUseCase> { DeleteUserUseCaseImpl(get()) }
    factory<GetUserUseCase> { GetUserUseCaseImpl(get()) }
    factory<UpdateUserUseCase> { UpdateUserUseCaseImpl(get()) }
}