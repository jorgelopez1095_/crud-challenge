package com.hugo.challenge.crud.interactor.usecase

import com.hugo.challenge.crud.data.UserEntity

interface GetUsersUseCase {
    suspend fun invoke(): List<UserEntity>
}