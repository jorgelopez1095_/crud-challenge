package com.hugo.challenge.crud.utils

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import kotlinx.coroutines.Dispatchers

abstract class BaseViewModelImpl: ViewModel(), BaseViewModel {

    protected fun exec (action: suspend () -> Unit) {
        launch(Dispatchers.Main) { action() }
    }

    val navigationEvent: MutableLiveData<SingleEvent<NavController.() -> Any>> = MutableLiveData()

    override fun navigateTo(route: Route, arguments: Bundle?) {
        withNavController { navigate(route.graph, arguments, defaultNavOptions) }
    }

    override fun navigateTo(route: RouteDestination,
                            arguments: Bundle?,
                            clearStack: Boolean,
                            navOptions: NavOptions?) {
        when {
            route is RouteDestination.Back -> withNavController { popBackStack() }
            clearStack -> withNavController { popBackStack(route.destination, false) }
            else -> withNavController { navigate(route.destination, arguments, navOptions) }
        }
    }

    private fun withNavController(block: NavController.() -> Any) {
        navigationEvent.postValue(SingleEvent(block))
    }

}