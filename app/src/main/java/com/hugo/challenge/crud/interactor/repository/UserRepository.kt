package com.hugo.challenge.crud.interactor.repository

import com.hugo.challenge.crud.data.UserEntity

interface UserRepository {
    suspend fun addNewUser(user: UserEntity)
    suspend fun getUserInformation(userId: Long): UserEntity?
    suspend fun getUsers(): List<UserEntity>
    suspend fun deleteUsers()
    suspend fun deleteUser(userId: Long)
    suspend fun updateUserInformation(
        userId: Long,
        userName: String,
        userSurName: String
    )
}