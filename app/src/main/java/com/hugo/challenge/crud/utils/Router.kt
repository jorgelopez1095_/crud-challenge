package com.hugo.challenge.crud.utils

import androidx.annotation.IdRes
import androidx.navigation.NavOptions
import com.hugo.challenge.crud.R

sealed class Route(@IdRes val graph: Int) {
    object Crud: Route(R.id.crud_nav_graph)
}

sealed class RouteDestination(@IdRes val destination: Int) {
    object Back: RouteDestination(-1)
    sealed class Crud(@IdRes destination: Int): RouteDestination(destination) {
        object SplashScreen: Crud(R.id.splashScreenFragment)
        object CrudScreen: Crud(R.id.crudFragment)
    }
}

val defaultNavOptions: NavOptions
    get() {
        return NavOptions.Builder()
            .build()
    }