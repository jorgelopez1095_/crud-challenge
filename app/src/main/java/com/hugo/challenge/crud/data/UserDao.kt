package com.hugo.challenge.crud.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveNewUser(user: UserEntity)

    @Query("SELECT * FROM $TABLE_USER")
    suspend fun users(): List<UserEntity>

    @Query("SELECT * FROM $TABLE_USER WHERE id=:userId")
    suspend fun user(userId: Long): UserEntity?

    @Query("UPDATE $TABLE_USER set name=:userName, sur_name=:userSurName WHERE id=:userId")
    suspend fun updateUser(userId: Long, userName: String, userSurName: String)

    @Query("DELETE FROM $TABLE_USER")
    suspend fun deleteUsers()

    @Query("DELETE FROM $TABLE_USER WHERE id=:userId")
    suspend fun deleteUser(userId: Long)
}