package com.hugo.challenge.crud.interactor.usecase

import com.hugo.challenge.crud.interactor.repository.UserRepository

class DeleteUsersUseCaseImpl (
    private val repository: UserRepository
): DeleteUsersUseCase {
    override suspend fun invoke() = repository.deleteUsers()
}