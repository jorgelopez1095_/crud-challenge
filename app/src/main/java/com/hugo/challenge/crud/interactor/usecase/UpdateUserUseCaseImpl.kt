package com.hugo.challenge.crud.interactor.usecase

import com.hugo.challenge.crud.interactor.repository.UserRepository

class UpdateUserUseCaseImpl (
    private val repository: UserRepository
): UpdateUserUseCase {
    override suspend fun invoke(userId: Long, userName: String, userSurName: String) =
        repository.updateUserInformation(userId, userName, userSurName)
}