package com.hugo.challenge.crud.utils

import android.os.Bundle
import androidx.navigation.NavOptions

interface BaseViewModel{
    fun onBackPressed() {}
    fun navigateTo(route: Route, arguments: Bundle? = null)
    fun navigateTo(route: RouteDestination,
                   arguments: Bundle? = null,
                   clearStack: Boolean = false,
                   navOptions: NavOptions? = defaultNavOptions)
}