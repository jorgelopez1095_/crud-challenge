package com.hugo.challenge.crud.utils

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView


/**
 * @author jorge-lopez / https://www.linkedin.com/in/jorge-lopez1095/
 *
 * T -> viewModel source.
 * X -> model of collection.
 */

class BaseAdapter <T: Any, X: Any> (
    private var viewModel: T,
    private var resource: Int):
    RecyclerView.Adapter<BaseAdapter<T, X>.CustomViewHolder>() {

    private var collections: MutableList<X>? = null
    private var viewDataBinding: ViewDataBinding? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        viewDataBinding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false)
        return CustomViewHolder(viewDataBinding!!)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    override fun getItemViewType(position: Int): Int {
        return resource
    }

    override fun getItemCount() = if (collections == null) 0 else collections?.size!!

    override fun getItemId(position: Int): Long = position.toLong()

    fun setData(collection: MutableList<X>?) {
        this.collections = collection
    }

    fun clearCollections() {
        this.collections?.clear()
    }

    fun setFilter(data: MutableList<X>) {
        this.collections = mutableListOf()
        collections?.addAll(data)
        this.notifyDataSetChanged()
    }

    inner class CustomViewHolder(private val binding: ViewDataBinding)
        : RecyclerView.ViewHolder(binding.root) {
        fun bind(viewModel: T, position: Int) {
            binding.run {
                setVariable(BR.viewModel, viewModel)
                setVariable(BR.position, position)
                executePendingBindings()
            }
        }
    }

}