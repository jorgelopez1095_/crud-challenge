package com.hugo.challenge.crud.interactor.repository

import com.hugo.challenge.crud.data.UserDao
import com.hugo.challenge.crud.data.UserEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepositoryImpl (
    private val dao: UserDao
): UserRepository {
    override suspend fun addNewUser(user: UserEntity) {
        withContext(Dispatchers.IO) {
            dao.saveNewUser(user)
        }
    }

    override suspend fun getUserInformation(userId: Long): UserEntity? =
        withContext(Dispatchers.IO) {
            return@withContext dao.user(userId)
        }

    override suspend fun getUsers(): List<UserEntity>  =
        withContext(Dispatchers.IO) {
            return@withContext dao.users()
        }

    override suspend fun deleteUsers() {
        withContext(Dispatchers.IO) {
            dao.deleteUsers()
        }
    }

    override suspend fun deleteUser(userId: Long) {
        withContext(Dispatchers.IO) {
            dao.deleteUser(userId)
        }
    }

    override suspend fun updateUserInformation(
        userId: Long,
        userName: String,
        userSurName: String
    ) {
        withContext(Dispatchers.IO) {
            dao.updateUser(userId, userName, userSurName)
        }
    }
}