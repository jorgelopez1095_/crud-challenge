package com.hugo.challenge.crud.interactor.usecase

import com.hugo.challenge.crud.data.UserEntity
import com.hugo.challenge.crud.interactor.repository.UserRepository

class AddUserUseCaseImpl (
    private val repository: UserRepository
): AddUserUseCase {
    override suspend fun invoke(user: UserEntity) = repository.addNewUser(user)
}