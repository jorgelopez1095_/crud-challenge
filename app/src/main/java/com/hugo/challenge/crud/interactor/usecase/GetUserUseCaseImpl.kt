package com.hugo.challenge.crud.interactor.usecase

import com.hugo.challenge.crud.data.UserEntity
import com.hugo.challenge.crud.interactor.repository.UserRepository

class GetUserUseCaseImpl (
    private val repository: UserRepository
): GetUserUseCase {
    override suspend fun invoke(userId: Long): UserEntity? = repository.getUserInformation(userId)
}