package com.hugo.challenge.crud.models

import androidx.lifecycle.viewModelScope
import com.hugo.challenge.crud.utils.BaseViewModelImpl
import com.hugo.challenge.crud.utils.RouteDestination
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

const val TIME_OUT_VIEW: Long = 2000

class SplashViewModel(): BaseViewModelImpl() {
    fun loadingSplashScreen() {
        viewModelScope.launch {
            delay(TIME_OUT_VIEW)
            navigateTo(RouteDestination.Crud.CrudScreen)
        }
    }
}