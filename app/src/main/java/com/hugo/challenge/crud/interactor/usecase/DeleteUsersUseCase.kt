package com.hugo.challenge.crud.interactor.usecase

interface DeleteUsersUseCase {
    suspend fun invoke()
}