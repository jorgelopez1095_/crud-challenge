package com.hugo.challenge.crud.interactor.usecase

import com.hugo.challenge.crud.data.UserEntity
import com.hugo.challenge.crud.interactor.repository.UserRepository

class GetUsersUseCaseImpl (
    private val repository: UserRepository
): GetUsersUseCase {
    override suspend fun invoke(): List<UserEntity> = repository.getUsers()
}