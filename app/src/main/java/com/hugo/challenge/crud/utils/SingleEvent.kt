package com.hugo.challenge.crud.utils

class SingleEvent <out T> (private val content: T) {

    var consumed = false
        private set // allow external read but not write

    /**
     * consumes the content if it's not been consumed yet.
     *
     * @return the unconsumed content or null if it was consumed already.
     */
    fun consume(): T? {
        return if (consumed) {
            null
        } else {
            consumed = true
            content
        }
    }

    /**
     * return the content data it's been handled or not.
     */
    fun peek() = content

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SingleEvent<*>

        if (content != other.content) return false
        if (consumed != other.consumed) return false

        return true
    }

    override fun hashCode(): Int {
        val result = content?.hashCode() ?: 0
        return 31 * result + consumed.hashCode()
    }

}