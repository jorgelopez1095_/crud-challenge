# C R U D | challenge

Challenges: separating the domain and presentation logic, managing mutation, building a non-trivial layout.

The task is to build a frame containing the following elements: a textfield Tprefix, a pair of textfields Tname and Tsurname, a listbox L, buttons BC, BU and BD and the three labels as seen in the screenshot. L presents a view of the data in the database that consists of a list of names. At most one entry can be selected in L at a time. By entering a string into Tprefix the user can filter the names whose surname start with the entered prefix—this should happen immediately without having to submit the prefix with enter. Clicking BC will append the resulting name from concatenating the strings in Tname and Tsurname to L. BU and BD are enabled iff an entry in L is selected. In contrast to BC, BU will not append the resulting name but instead replace the selected entry with the new name. BD will remove the selected entry. The layout is to be done like suggested in the screenshot. In particular, L must occupy all the remaining space.
[DOWNLOAD APK](https://drive.google.com/file/d/1TvAOUYY4Pivj3eKfrDtLOUdR6uTwzxhr/view?usp=sharing)

### SCREENSHOTS
-----------

![init-view](screenshots/crud_i.jpeg "Main-view application")

### LANGUAGES, LIBRARIES AND TOOLS USED

* [Kotlin](https://kotlinlang.org/)
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)
* [DataBinding](https://developer.android.com/topic/libraries/view-binding?hl=es-419)
* [Arch Lifecycle](https://developer.android.com/reference/android/arch/lifecycle/package-summary)
* [ConstraintLayouts](https://developer.android.com/training/constraint-layout)
* [Coroutines](https://developer.android.com/kotlin/coroutines)
* [Room Persistance](https://developer.android.com/jetpack/androidx/releases/room)
* [Lottie](https://github.com/LottieFiles/lottie-android)
* [RecyclerView](https://developer.android.com/jetpack/androidx/releases/recyclerview)
* [KOIN](https://insert-koin.io/)
* [Navigation Component](https://developer.android.com/guide/navigation/navigation-getting-started)
* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)

### REQUIREMENTS

* JDK 1.8
* [Android SDK](https://developer.android.com/studio/index.html)
* [Kotlin Version](https://kotlinlang.org/docs/releases.html)
* [MIN SDK 23](https://developer.android.com/preview/api-overview.html))
* [Build Tools Version 30.0.3](https://developer.android.com/studio/releases/build-tools)
* Latest Android SDK Tools and build tools.

### ARCHITECTURE

In this project, I tried to cover various areas of technology, such as data persistence, design patterns, principles of single responsibility, among others.

- SOLID Abstractions.
- Dependency Injection.
- MVVM.

## L I C E N S E

Under the [MIT license](https://opensource.org/licenses/MIT).